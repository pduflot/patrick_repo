import {Component, OnInit} from "@angular/core";
import {UserService} from "../services/user/user.service";
import {Router} from '@angular/router';

@Component({
  selector: 'ibapt-header',
  templateUrl: './ibapt-header.component.html',
  styleUrls: ['./ibapt-header.component.css']
})
export class HeaderComponent implements OnInit {

  username: string;
  password: string;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
  }

  showAccountDetails(): void {
    //TODO
  };

  navigateToHome() {
     this.router.navigate(['home']);
  }
}
