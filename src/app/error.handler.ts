import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { AlertService } from './services/alert/alert.service';

@Injectable()
export class GlobalErrorHandler extends ErrorHandler {
   constructor(private injector: Injector) {
      super();
   }

   handleError(error) {
      const alertService = this.injector.get(AlertService);
      const message = error.message ? error.message : error.toString();
      alertService.error(message);
      super.handleError(error);
   };
}