import { Component, OnInit } from '@angular/core';
import { RightService } from '../services/right/right.service';
import { Right, Group, Grant } from '../model/data';
import { AlertService } from 'app/services/alert/alert.service';

@Component({
   selector: 'home',
   styleUrls: ['./home.component.css'],
   templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit 
{
   private rights:Right[];
   private selectedRight:Right;
   private selectedRole:Group;
   private roles:Group[];
   private grants:Grant[];
   private unknowns:Right[];

   constructor(private rightService:RightService, private alertService: AlertService) 
   {
   }

   public ngOnInit() {
      this.rightService.getAllRights().subscribe(
         data => { this.rights = data; },
         err => { throw err; }
      );
      this.updateUnknowns();
      this.rightService.getAllRoles().subscribe(
         data => { this.roles = data; },
         err => { throw err; }
      )
   }

   private updateUnknowns() {
      this.rightService.getAllUnknowns().subscribe(
         data => { 
            this.unknowns = data.length>0 ? data : undefined; 
         },
         err => { throw err; }
      );
   }

   onRightTabSelected()
   {
      this.onRightSelected(this.selectedRight);
   }

   onRoleTabSelected()
   {
      this.onRoleSelected(this.selectedRole);
   }

   onRightSelected(right: Right)
   {
      this.selectedRight = right;
      if (this.selectedRight) {
         this.rightService.getGrantsByRight(right.id).subscribe(
            data => { this.grants = data; },
            err => { throw err; }
         );
      }
      else {
         this.grants = undefined;
      }
   }

   onRoleSelected(role: Group)
   {
      this.selectedRole = role;
      if (this.selectedRole) {
         this.rightService.getGrantsByRole(role.name).subscribe(
            data => { this.grants = data; },
            err => { throw err; }
         );
      }
      else {
         this.grants = undefined;
      }
   }

   onSaveRight()
   {
      let grants = this.grants.filter(grant => grant.granted);
      this.rightService.saveGrantsForRight(this.selectedRight.id,
         grants.map(grant => grant.group.name)
      ).subscribe(
         data => { 
            this.alertService.info(`Saved grants for right ${this.selectedRight.mnemonic}.`);
            grants.forEach(grant => { grant.grantDate = new Date().toLocaleString() });
            this.updateUnknowns();
         },
         err => { 
            throw err; 
         }
      );
   }

   onSaveRole()
   {
      let grants = this.grants.filter(grant => grant.granted);
      this.rightService.saveGrantsForRole(this.selectedRole.name,
         grants.map(grant => grant.right.id)
      ).subscribe(
         data => { 
            this.alertService.info(`Saved grants for role ${this.selectedRole.name}.`);
            grants.forEach(grant => { grant.grantDate = new Date().toLocaleString() });
         },
         err => { 
            throw err; 
         }
      );
   }
   
   editUnknown()
   {
      for (let b1=0; b1<this.rights.length; b1++) {
         if (this.rights[b1].id===this.unknowns[0].id) {
            this.onRightSelected(this.rights[b1]);
            break;
         }
      }
   }
}
