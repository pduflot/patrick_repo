import { NO_ERRORS_SCHEMA } from '@angular/core';
import { inject, async, TestBed, getTestBed, ComponentFixture } from '@angular/core/testing';
import { Component } from '@angular/core';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { HomeComponent } from './home.component';

describe(`Home`, () => {
   let comp: HomeComponent;
   let fixture: ComponentFixture<HomeComponent>;
   let injector: TestBed; 
   let httpMock: HttpTestingController 
   
   /**
    * async beforeEach.
    */
   beforeEach(async(() => {
      TestBed.configureTestingModule({
         imports: [HttpClientTestingModule],
         declarations: [HomeComponent],
         schemas: [NO_ERRORS_SCHEMA],
         providers: [HttpTestingController]
      }).compileComponents(); // Compile template and CSS
      injector = getTestBed(); 
      httpMock = injector.get(HttpTestingController); 
   }));

   /**
    * Synchronous beforeEach.
    */
   beforeEach(() => {
      fixture = TestBed.createComponent(HomeComponent);
      comp = fixture.componentInstance;

      /**
       * Trigger initial data binding.
       */
      fixture.detectChanges();
   });

   it('should log ngOnInit', () => {
      spyOn(console, 'log');
      expect(console.log).not.toHaveBeenCalled();

      comp.ngOnInit();
      expect(console.log).toHaveBeenCalled();
   });

});
