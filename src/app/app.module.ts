/* Angular imports */
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {ErrorHandler} from '@angular/core';
import { FlexLayoutModule } from "@angular/flex-layout";
import {
   NgModule,
   ApplicationRef
} from '@angular/core';
import {
   RouterModule,
   PreloadAllModules
} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/* Other cots imports */
import { AlertModule, TabsModule } from 'ngx-bootstrap';
import { JwtModule } from '@auth0/angular-jwt';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';

// Application components
import { AppComponent } from './app.component';
import { HomeComponent } from './home';
import { NoContentComponent } from './no-content';
import { XLargeDirective } from './home/x-large';
import { HeaderComponent } from "./ibapt-header/ibapt-header.component";
import { FooterComponent } from "./ibapt-footer/ibapt-footer.component";
import { LoginComponent } from './login/login.component';
import { AlertBarComponent } from './alert-bar/alert-bar.component';
// Application services
import { AuthenticationService } from "./services/authentication/authentication.service";
import { AuthGuard } from "./guards/auth-guard.service";
import { RightService } from "./services/right/right.service";
import { UserService } from "./services/user/user.service";
import { AlertService } from './services/alert/alert.service';
import { TOKEN_NAME } from './services/authentication/auth.constant';
import { GlobalErrorHandler } from './error.handler';

import '../styles/styles.scss';
import '../styles/headings.css';

// Application wide providers
const APP_PROVIDERS = [
   ...APP_RESOLVER_PROVIDERS,
   AuthenticationService,
   RightService,
   UserService,
   AlertService,
   AuthGuard,
   {provide: ErrorHandler, useClass: GlobalErrorHandler}
];

let WHITELIST;
if ('production'!==ENV)
   WHITELIST = [ 'localhost:8081' ];

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
   bootstrap: [AppComponent],
   declarations: [
      AppComponent,
      HomeComponent,
      NoContentComponent,
      HeaderComponent,
      FooterComponent,
      XLargeDirective,
      LoginComponent,
      AlertBarComponent
   ],
   /**
    * Import Angular's modules.
    */
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      FormsModule,
      HttpClientModule,
      FlexLayoutModule,
      RouterModule.forRoot(ROUTES, {
         useHash: Boolean(history.pushState) === false,
         preloadingStrategy: PreloadAllModules
      }),
      JwtModule.forRoot({
         config: {
            tokenGetter: () => {
               return localStorage.getItem(TOKEN_NAME);
            }
         }
      }),
      AlertModule.forRoot(),
      TabsModule.forRoot()
   ],
   /**
    * Expose our Services and Providers into Angular's dependency injection.
    */
   providers: [
      ENV_PROVIDERS,
      APP_PROVIDERS
   ]
})
export class AppModule {

   constructor(
      public appRef: ApplicationRef,
   ) { }
}
