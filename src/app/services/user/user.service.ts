import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';
import { TOKEN_NAME, USER_NAME } from '../../services/authentication/auth.constant';

@Injectable()
export class UserService {
   userName: string;
   accessToken: string;
   isAdmin: boolean;

   constructor(private router: Router, private jwtHelper: JwtHelperService) {
      let accessToken = localStorage.getItem(TOKEN_NAME);
      let userName = localStorage.getItem(USER_NAME);
      this.parseToken(userName, accessToken);
   }

   login(userName: string, accessToken: string) {
      this.parseToken(userName, accessToken);
      localStorage.setItem(TOKEN_NAME, accessToken);
      localStorage.setItem(USER_NAME, userName);
   }

   private parseToken(userName: string, accessToken: string)
   {
      if (accessToken)
      {
         const decodedToken = this.jwtHelper.decodeToken(accessToken);
         console.log(decodedToken);
         this.isAdmin = decodedToken.authorities.some(el => el === 'ADMIN_USER');
      }
      this.userName = userName;
      this.accessToken = accessToken;
   }

   logout() {
      this.userName = null;
      this.accessToken = null;
      this.isAdmin = false;
      localStorage.removeItem(TOKEN_NAME);
      this.router.navigate(['login']);
   }

   navigateToHome()
   {
      this.router.navigate(['home']);
   }

   isAuthenticated(): boolean {
      return this.accessToken ? true : false;
   }

   isAdminUser(): boolean {
      return this.isAuthenticated() && this.isAdmin;
   }

   isUser(): boolean {
      return this.isAuthenticated() && !this.isAdmin;
   }

   getUserName(): string {
      return this.userName;
   }
}