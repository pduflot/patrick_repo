import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, getTestBed, inject } from '@angular/core/testing';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
   let injector: TestBed;
   let service: AuthenticationService;
   let httpMock: HttpTestingController;

   beforeEach(() => {
      TestBed.configureTestingModule({
         schemas: [NO_ERRORS_SCHEMA],
         imports: [HttpClientTestingModule],
         providers: [HttpTestingController, AuthenticationService]
      });
      injector = getTestBed();
      service = injector.get(AuthenticationService);
      httpMock = injector.get(HttpTestingController);
   });

   it('should ...', inject([AuthenticationService], (service: AuthenticationService) => {
      expect(service).toBeTruthy();
   }));
});
