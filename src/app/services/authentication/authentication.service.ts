import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { TOKEN_AUTH_PASSWORD, TOKEN_AUTH_USERNAME } from './auth.constant';

export interface LoginResponse {
   user: string;
   token: any;
}

@Injectable()
export class AuthenticationService {
   constructor(private http: HttpClient) {
   }

   login(username: string, password: string) : Observable<LoginResponse>
   {
      const body = `username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}&grant_type=password`;

      const headers = new HttpHeaders();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD));

      var stubbed : LoginResponse = { user:username, token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6InBvbCIsImF1dGhvcml0aWVzIjpbIkFETUlOX1VTRVIiXX0.1KAhxX-R6mOTItVavhwiF9MZcGJdbKpxEGXWTqnPCg0'};
      return Observable.of(stubbed);
   }
}