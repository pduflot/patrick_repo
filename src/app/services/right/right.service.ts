import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Group, Right, Grant } from '../../model/data';

interface JsonGrant
{
   group:Group; right:Right; grantDate:string;
}

@Injectable()
export class RightService {
   constructor(private http: HttpClient) {
   }

   getAllRights(): Observable<Right[]>
   {
      let url:string = 'api/right';
      return this.http.get<Right[]>(url);
   }

   getAllUnknowns(): Observable<Right[]>
   {
      let url:string = 'api/right/unknown';
      return this.http.get<Right[]>(url);
   }

   getGrantsByRight(rightId: number): Observable<Grant[]>
   {
      let url:string = 'api/right/' + rightId + '/roles';
      return this.http.get<JsonGrant[]>(url)
         .map<JsonGrant[], Grant[]>((grants:JsonGrant[]) => {
            return grants.map(grant => new Grant(grant.group, grant.right, grant.grantDate));
         });
   }

   saveGrantsForRight(rightId: number, groups: string[])
   {
      let url:string = 'api/right/' + rightId + '/roles';
      return this.http.post(url, groups);
   }

   getAllRoles(): Observable<Group[]>
   {
      let url:string = 'api/role';
      return this.http.get<Group[]>(url);
   }
   
   getGrantsByRole(roleId: string): Observable<Grant[]>
   {
      let url:string = 'api/role/' + roleId + '/rights';
      return this.http.get<JsonGrant[]>(url)
         .map<JsonGrant[], Grant[]>((grants:JsonGrant[]) => {
            return grants.map(grant => new Grant(grant.group, grant.right, grant.grantDate));
         });
   }

   saveGrantsForRole(roleId: string, rights: number[])   
   {
      let url:string = 'api/role/' + roleId + '/rights';
      return this.http.post(url, rights);
   }
}
