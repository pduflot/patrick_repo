export class Right 
{
   public constructor(public id: number, public mnemonic: string, public description: string)
   {}
}

export class Group 
{
   constructor(public name:string, public description: string, public missing:boolean) {
   }
}

export class Grant
{
   public granted:boolean;
   constructor(public group:Group, public right:Right, public grantDate:string) {
      this.granted = !!this.grantDate;
   }
}