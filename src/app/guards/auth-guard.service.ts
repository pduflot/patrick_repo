import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';
import { JwtHelperService } from '@auth0/angular-jwt';

import { TOKEN_NAME } from '../services/authentication/auth.constant';
import { UserService } from '../services/user/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
   constructor(private router: Router, private userService: UserService, private jwtHelper: JwtHelperService) {
   }

   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      if (this.userService.accessToken && !this.jwtHelper.isTokenExpired(this.userService.accessToken)) {
         return true;
      } else {
         this.router.navigate(['login'], { queryParams: { redirectTo: state.url } });
         return false;
      }
   }
}