- Search and replace all "AngularClass" and "starter" in .json, .js, .ts, .html
- Copy application src/app to seed
- Copy application src/asset to seed
- Insert any additional dependencies in package.json
- Insert any additional dev dependencies in package.json
- If using SCSS, switch to scss as explained here: https://github.com/gdi2290/angular-starter/wiki/How-to-include-SCSS-in-components
- Set up bootstrap SaSS as explained here: https://github.com/gdi2290/angular-starter/wiki/How-to-use-Bootstrap-3-and-Sass-and-ngx-bootstrap
- Setup proxies for dev server in webpack.dev.ts:
   devServer: { ...
      proxy: {
         "/api": {
            target:"http://localhost:8081"
         },
         "/icomp": {
            target:"http://localhost:8081",
            ws: true
         }
      }...
